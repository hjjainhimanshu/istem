'use strict';

const Sequelize = require('sequelize');
const sequelize = require('./index');
module.exports = function(sequelize, DataTypes) {

  const Email = sequelize.define('Email', {
  	subject: {
      type: Sequelize.STRING(300)
    },
    body: {
      type: Sequelize.STRING,
      allowNull: false
    },
    status: {
    	type: Sequelize.INTEGER, // sent, draft, read - can accomodate later stages also
    	defaultValue: 0, 
    },
    userId: {
    	type: Sequelize.INTEGER,
      allowNull: false
    }
  }, {
    timestamps: true,
    tableName: "email",
    classMethods: {
      associate: function (models) {
        models.Email.belongsTo(models.User, {foreignKey: "userId"});
      }
    }
  });
  
  return Email;
}