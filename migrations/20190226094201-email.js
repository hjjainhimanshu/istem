'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {

    return queryInterface.createTable('email', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      subject: {
        type: Sequelize.STRING(300)
      },
      body: {
        type: Sequelize.STRING,
        validate: {
          notNull: true
        }
      },
      status: {
        type: Sequelize.INTEGER, // sent, draft, read - can accomodate later stages also
        defaultValue: 0, 
      },
      userId: {
        type: Sequelize.INTEGER,
        references: {
          model: "user",
          key: "id"
        },
        onUpdate: 'cascade',
        onDelete: 'restrict'
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE
    });
  },

  down: function (queryInterface, Sequelize) {

    return queryInterface.dropTable('email');
  }
};
