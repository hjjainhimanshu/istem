/**
 * GET /*
 * Home page.
 */

'use strict';
const User = require('../models/user');

/**
 *
 * @type {{index: Function}}
 */
module.exports = {
  index: function (req, res, next) {
    return res.render('index', {title: 'Istem', user: req.user, message: 'Hello '+req.user.firstName});
  },
  login: function (req, res, next) {
    return res.render('login');
  },
};
