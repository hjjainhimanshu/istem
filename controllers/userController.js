/**
 * GET /*
 * Home page.
 */

'use strict';
const models = require('../models/');

/**
 *
 * @type {{index: Function}}
 */
module.exports = {
  register: function (req, res, next) {
  	models.User.create(req.body).then(user => {
      return res.render('index', { title: 'Istem', user: user, message: 'Hello '+user.firstName});
  	}) 
  },
  update: function (req, res, next) {
    models.User.update(req.body, {where: {id:req.params.id}}).then(user => {
      req.session.passport.user = req.body;
      req.session.save(function(err) {console.log(err);});
      return res.render('index', { title: 'Istem', user: user, message: 'Hello '+user.firstName});
    })
  }
};
