/**
 * GET /*
 * Home page.
 */

'use strict';
const models = require('../models/');
const mailer = require('nodemailer-promise');
const aws = require('aws-sdk');

var sendEmail = mailer.config({
  service: 'ses',
  SES: new aws.SES({
    apiVersion: '2010-12-01',
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY, 
    sslEnabled: true,
    region: process.env.REGION
  })
});
/**
 *
 * @type {{index: Function}}
 */
module.exports = {
  email: function (req, res, next) {
  	models.User.findAll({where: {userType: req.body.userType}}).then(users => {
      var promiseArr = []
      for(var index in users){
        promiseArr.push(
          sendEmailPromise({
            from: process.env.FROM_EMAIL,
            to: users[index].email,
            subject: req.body.subject,
            html: req.body.body
          })
        );
      }
      Promise.all(promiseArr).then(function(data) {
        return res.render('index', {title: 'Istem', user: req.user, message: promiseArr.length +' Emails Sent' });
      }).catch(function(err){
        return res.render('index', {title: 'Istem', user: req.user, message: 'Email Failure'});
      });
    });
  }
};

var sendEmailPromise = function(message) {
  return new Promise(function (resolve, reject) {
    sendEmail(message)
      .then(function(info){console.log(info); resolve(info);})   // if successful
      .catch(function(err){console.log('got error'); reject(err)}); 
  });
}
