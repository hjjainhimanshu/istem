'use strict';
var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;
var models = require('../models');

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
  }, function(username, password, done) {
    models.User.findOne({where:{ email: username }})
    .then(user => {
      if (!user) {
        return done(null, false);
      }
      if (!user.comparePassword(password)) {
        return done(null, false);
      }
      return done(null, user);
    }).catch(err => { 
      if (err) { return done(err); }
    });
  }
));
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

module.exports = passport;